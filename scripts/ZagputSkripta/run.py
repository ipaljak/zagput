import string
import random
import os
import re
import subprocess

images = [
    'task-01-a.png',
    'task-02-a.png',
    'task-02-b.png',
    'task-03-a.png',
    'task-04-a.png',
    'task-04-b.png',
    'task-05-a.png',
    'task-06-a.png',
    'task-07-a.jpg',
    'task-07-b.jpg',
    'task-08-a.png',
    'task-08-b.png',
    'task-09-a.png',
    'task-10-a.png',
    'task-11-a.png',
    'task-12-a.png',
    'task-13-a.jpg',
    'task-13-b.jpg',
    'task-14-a.png',
    'task-15-a.png',
    'task-16-a.png',
    'task-16-b.png',
    'task-17-a.png',
    'task-17-b.png',
    'task-17-L.png',
    'task-18-a.png',
    'task-18-b.png',
    'task-18-c.png',
    'task-18-d.png',
    'task-18-e.png',
    'task-18-hash.png',
    'task-19-a.png',
    'task-20-a.png',
    'task-21-a.png',
    'task-22-a.png',
    'task-23-a.png',
    'task-24-a.png',
    'task-24-b.png',
    'task-24-c.png',
    'task-24-d.png',
    'task-25-a.png'
]

FILE_NAME_REGEX = "\d+\.js"
QUERY_TEMPLATE = "UPDATE images SET title = \'{newName}\' WHERE title = \'{oldName}\';\n"

# Project config
PROJECT_DIR_PATH = "/home/mjurinic/PhpStormProjects/zagput"
QUERY_FILE_NAME = "queries.txt"

# Database config
DATABASE_NAME = "zagput_local"
DATABASE_USERNAME = "root"
DATABASE_PASSWORD = "toor"

randomImageNames = {}
dbQueries = []

# Have to keep the original order...
counter = 10

def generateRandomName():
    global counter

    randomSet = string.ascii_uppercase + string.ascii_lowercase + string.digits
    counter += 1

    return 'x' + str(counter) + ''.join(random.sample(randomSet * 64, 64))


if __name__ == "__main__":
    # Generate random names
    for image in images:
        randomName = generateRandomName()

        if image == "task-18-hash.png":
            randomName = image.split('.')[0]

        dbQueries.append(QUERY_TEMPLATE.format(newName = randomName + '.' + image.split('.')[1], oldName = image))
        randomImageNames.update({image : randomName})


    # Replace real names with randoms in {taskNumber}.js files
    for fileName in os.listdir(PROJECT_DIR_PATH + "/public/js/"):
        if re.match(FILE_NAME_REGEX, fileName):
            with open(PROJECT_DIR_PATH + "/public/js/" + fileName, "rt") as fin:
                with open(os.getcwd() + "/js/" + fileName, "wt") as fout:
                    for line in fin:
                        imageNames = []

                        for image in images:
                            if image.split('.')[0] in line:
                                imageNames.append(image)

                        if len(imageNames) != 0:
                            for i in imageNames:
                                line = line.replace(i.split('.')[0], randomImageNames.get(i))

                        fout.write(line)


    # Replace old images with new ones
    for image in images:
        print "[Renaming] " +  PROJECT_DIR_PATH + "/public/images/" + image + " -> " + PROJECT_DIR_PATH + "/public/images/" + randomImageNames.get(image) + '.' + image.split('.')[1]
        subprocess.Popen("mv " + PROJECT_DIR_PATH + "/public/images/" + image + " " + PROJECT_DIR_PATH + "/public/images/" + randomImageNames.get(image) + '.' + image.split('.')[1], shell = True)


    # Replace old audio file for task-21
    print "[Renaming] " +  PROJECT_DIR_PATH + "/public/audio/task-21-a.wav" + " -> " + PROJECT_DIR_PATH + "/public/audio/" + randomImageNames.get("task-21-a.png") + '.wav'
    subprocess.Popen("mv " + PROJECT_DIR_PATH + "/public/audio/task-21-a.wav" + " " + PROJECT_DIR_PATH + "/public/audio/" + randomImageNames.get("task-21-a.png") + '.wav', shell = True)


    # Replace old scripts with new ones
    for fileName in os.listdir(os.getcwd() + "/js/"):
        if re.match(FILE_NAME_REGEX, fileName):
            print "[Deleting] " + PROJECT_DIR_PATH + "/public/js/" + fileName
            subprocess.call("rm " + PROJECT_DIR_PATH + "/public/js/" + fileName, shell = True)

            print "[Moving] " + os.getcwd() + "/js/" + fileName + " -> " + PROJECT_DIR_PATH + "/public/js/" + fileName
            subprocess.call("mv " + os.getcwd() + "/js/" + fileName + " " + PROJECT_DIR_PATH + "/public/js/" + fileName, shell = True)


    # Output queries to 'queries.txt'
    with open(QUERY_FILE_NAME, "wt") as fout:
        for query in dbQueries:
            fout.write(query)


    # Migrate, seed and update our DB with new image names
    print "\nBegin database update..."

    # !!!!!!!!  DO NOT RUN IN PRODUCTION !!!!!!!!
    print subprocess.check_output("php " + PROJECT_DIR_PATH + "/artisan migrate:refresh", shell = True)
    # !!!!!!!!  DO NOT RUN IN PRODUCTION !!!!!!!!

    print subprocess.check_output("php " + PROJECT_DIR_PATH + "/artisan db:seed", shell = True)
    print subprocess.check_output("mysql -u " + DATABASE_USERNAME + " -p" + DATABASE_PASSWORD + " " + DATABASE_NAME + " < " + os.getcwd() + "/" + QUERY_FILE_NAME, shell = True)
