@extends('templates.master')

@section('title', 'Kuća slavnih')

@section('content')
    <table class="highlight centered">
        <thead>
        <tr>
            <th>Rang</th>
            <th>Putnik</th>
            <th>Stanica</th>
        </tr>
        </thead>
        <tbody>
        @for ($i = 0; $i < count($ranks); ++$i)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $ranks[$i]->username }}</td>
                <td>
                    @if ($ranks[$i]->task_id + 1 == 26)
                        <img src="/images/crown.png">
                    @elseif ($ranks[$i]->correct_answer == 1)
                        {{ $ranks[$i]->task_id + 1 }}
                    @else
                        {{ $ranks[$i]->task_id }}
                    @endif
                </td>
            </tr>
        @endfor
        </tbody>
    </table>
@endsection