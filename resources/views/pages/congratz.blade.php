@extends('templates.master')

@section('title', 'Čestitamo!')

@section('content')
    <div class="container center">
        <img class="responsive-img" src="/images/end-screen.jpg" alt="Čestitamo">
    </div>
@endsection