@extends('templates.master')

@section('title', $title)

@if ($id != 18)
    @section('nav-title', '#' . $id)
@else
    @section('nav-title', '<img id="task-18-hash" style="margin-top: 17px;" src="/images/task-18-hash.png" usemap="#task-18-hash-map">')
    @section('style', 'area { cursor: pointer; }')
@endif

@section('content')
    <div class="container center">
        <div class="task valign-wrapper">
            @for ($i = 0; $i < sizeof($images); ++$i)
                @if ($i != 0)
                    <div id="{!! (explode('.', $images[$i]['title']))[0] !!}-img" class="hide img-task-holder">
                        <div class="{!! (explode('.', $images[$i]['title']))[0] !!}-img" style="position: relative; display: inline-block;">
                            <img class="{!! (explode('.', $images[$i]['title']))[0] !!}-img-tag responsive-img valign center-align"
                                 src="/images/{!! $images[$i]['title'] !!}" usemap="#{!! (explode('.', $images[$i]['title']))[0] !!}-map">
                        </div>
                    </div>
                @else
                    <div id="{!! (explode('.', $images[$i]['title']))[0] !!}-img" class="img-task-holder">
                        <div class="{!! (explode('.', $images[$i]['title']))[0] !!}-img" style="position: relative; display: inline-block;">
                            <img class="{!! (explode('.', $images[$i]['title']))[0] !!}-img-tag responsive-img valign center-align"
                                 src="/images/{!! $images[$i]['title'] !!}" usemap="#{!! (explode('.', $images[$i]['title']))[0] !!}-map">
                        </div>
                    </div>
                @endif
            @endfor
        </div>
        <div class="answer">
            <div class="row">
                <form class="col s12" action="" method="POST">
                    <input id="registration-csrf-token" type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="col s8 offset-s1 l6 offset-l3">
                        <div class="input-field">
                            <label for="answer">Lozinka</label>
                            <input type="text" id="answer" name="answer">
                        </div>
                    </div>

                    <button id="btn-answer" class="clear left blue darken-2 waves-effect waves-light btn col s2 l1">&gt;&gt;</button>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js-section')
    @if (isset($script))
        <script type="text/javascript" src="/js/{!! $script !!}"></script>
    @endif
@endsection