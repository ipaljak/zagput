@extends('templates.master')

@section('title', 'Zagonetke')

@section('content')
<br>
<div class="container">
    <ul class="collapsible popout">
        @foreach($puzzles as $puzzle)
            <li>
                <div class="collapsible-header">
                    <div class="row">
                        <div class="col m1"><p><b>[{{ $loop->iteration }}]</b></p></div>
                        <div class="col m10"><p>{{ $puzzle->question }}</p></div>
                        <div class="col m1 hide-on-small-only"><i id="puzzle_icon{{ $loop->iteration }}" class="right material-icons">expand_more</i></div>
                    </div>
                </div>
                <div class="collapsible-body"><p>{{ $puzzle->answer }}</p></div>
            </li>
        @endforeach
    </ul>
</div>
@endsection