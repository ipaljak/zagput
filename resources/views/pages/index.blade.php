@extends('templates.master')

@section('title', 'Zagonetni put')

@section('content')
    <div class="container center">
        <img src="images/zagput-2.png" class="responsive-img">
        <div style="bottom: 0px;">
            <p class="flow-text">Pokaži da si pametniji od drugih i<br>završi zagonetno<br>putovanje!</p>
            <button data-target="start-modal" class="blue darken-2 waves-effect waves-light btn start-modal-trigger">START</button>
        </div>
    </div>

    <!-- Modal -->
    <div id="start-modal" class="modal">
        <div class="modal-content">
            <ul id="modal-tabs" class="tabs">
                <li id="btn-tab-login" class="tab"><a href="#tab-login">Prijava</a></li>
                <li id="btn-tab-registration" class="tab"><a href="#tab-registration">Registracija</a></li>
            </ul>

            <div id="preloader" class="container center hide">
                <div class="row">
                    <div class="col s12">
                        <div class="preloader-wrapper big active">
                            <div class="spinner-layer spinner-blue-only center-align">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="gap-patch">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Tabs -->
            <div id="tab-login">
                <form id="form-login" action="">
                    <input id="login-csrf-token" type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div id="login-error" class="input-field hide red-text">
                        <p class="center">Krivo korisničko ime ili lozinka.</p>
                    </div>

                    <div id="login-fields">
                        <div class="input-field">
                            <input type="text" name="username" id="login-username" placeholder="Korisničko ime">
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" id="login-password" placeholder="Lozinka">
                        </div>

                        <button id="btn-login" class="clear right blue darken-2 waves-effect waves-light btn">PRIJAVA</button>

                        <div class="clear-both"></div>
                    </div>
                </form>
            </div>

            <div id="tab-registration">
                <form id="form-registration" action="">
                    <input id="registration-csrf-token" type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div id="registration-error" class="input-field hide red-text">
                        <p class="center">Došlo je do pogreške tijekom registracije.</p>
                    </div>

                    <div id="registration-success" class="input-field hide green-text">
                        <p class="center">Registracija uspješna!</p>
                    </div>

                    <div id="register-fields">
                        <div class="input-field">
                            <input type="text" name="username" id="reg-username" placeholder="Korisničko ime">
                            <label for="reg-username" data-error="Polje je obavezno."></label>
                        </div>
                        <div class="input-field">
                            <input type="password" name="password" id="reg-password" placeholder="Lozinka">
                        </div>
                        <div class="input-field">
                            <input type="password" name="password_confirmation" id="reg-repassword" placeholder="Ponovi lozinku">
                        </div>

                        <button id="btn-register" class="clear right blue darken-2 waves-effect waves-light btn">POTVRDI</button>

                        <div class="clear-both"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('js-section')
    @if(App::environment('production'))
        <script type="text/javascript" src="js/index.min.js"></script>
    @else
        <script type="text/javascript" src="js/index.js"></script>
    @endif
@endsection
