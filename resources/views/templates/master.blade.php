<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="keywords" content="zagonetni,put,zagonetni put,zagonetke,zagonetka,zagonetno,zagonetno putovanje,putovanje,test,iq,iq test,kviz,iq kviz,zabava,misliti,razmisljanje,zadaci,mozgalica,mozganje,pamet,mozak">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>@yield('title')</title>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/css/materialize.min.css" media="screen, projection"/>

    @if(App::environment('production'))
        <link type="text/css" rel="stylesheet" href="/css/master.min.css" media="screen, projection"/>
    @else
        <link type="text/css" rel="stylesheet" href="/css/master.css" media="screen, projection"/>
    @endif

    <style>@yield('style')</style>
</head>
<body>
    <nav>
        <div class="grey lighten-3 nav-wrapper">
            <a id="title" href="#!" class="brand-logo center">@yield('nav-title')</a>
            <a href="#" data-activates="mobile-menu" class="left button-collapse"><i class="material-icons">menu</i></a>
            <ul class="left hide-on-med-and-down">
                <li><a href="/"><i class="large material-icons left blue-grey-text">home</i>Početna</a></li>
                <li><a href="#rules" class="modal-trigger"><i class="large material-icons left blue-grey-text">gavel</i>Pravila</a></li>
                <li><a href="/hall-of-fame"><i class="large material-icons left blue-grey-text">grade</i>Kuća slavnih</a></li>
                <li><a href="/puzzles"><i class="large material-icons left blue-grey-text">games</i>Zagonetke</a></li>
            </ul>
            @if (Auth::check())
                <ul class="right hide-on-med-and-down">
                    <li><a href="/"><i class="large material-icons left blue-grey-text">account_circle</i>{{ Auth::user()->username }}</a></li>
                    <li><a href="/logout"><i class="large material-icons left blue-grey-text">exit_to_app</i>Odjava</a></li>
                </ul>
            @endif
            <ul class="left side-nav" id="mobile-menu">
                @if (Auth::check())
                    <br>
                    <li><a href="/"><i class="large material-icons left blue-grey-text">account_circle</i>{{ Auth::user()->username }}</a></li>
                    <br><hr>
                @endif
                <li><a href="/"><i class="large material-icons left blue-grey-text">home</i>Početna</a></li>
                <li><a href="#rules" class="modal-trigger"><i class="large material-icons left blue-grey-text">gavel</i>Pravila</a></li>
                <li><a href="/hall-of-fame"><i class="large material-icons left blue-grey-text">grade</i>Kuća slavnih</a></li>
                <li><a href="/puzzles"><i class="large material-icons left blue-grey-text">games</i>Zagonetke</a></li>
                @if (Auth::check())
                    <li><a href="/logout"><i class="large material-icons left blue-grey-text">exit_to_app</i>Odjava</a></li>
                @endif
            </ul>
        </div>
    </nav>

    @yield('content')

    <div id="rules" class="modal">
        <div class="modal-content">
            <h5>Pravila</h5>
            <hr>
            <p>
                Zagonetni put je igra u kojoj je cilj uspješno proći što više zagonetnih stanica.
                Svaka je stanica jedna zagonetka čije je rješenje lozinka kojom ćete otići na sljedeću stanicu. <br><br>

                Zagonetka se nalazi u središnjem prostoru ispod trake s linkovima i brojem stanice te iznad prostora u koji se upisuje lozinka.
                Bitno je napomenuti da i naslov kartice određene stanice može biti vrlo koristan pri rješavanju zagonetke.
                <br><br>

                Lozinke se pišu prema pravopisu što znači da se sva imena pišu velikim početnim slovom, a riječi koje nisu imena pišu se malim početnim slovom.
                <br><br>

                Npr. pravilno napisane lozinke su: “Slavonski Brod” , “ruksak” ili “Trg bana Josipa Jelačića”. <br><br>

                Iznimka su zagonetke u kojima je navedeno ili pokazano da se lozinka ne piše po pravopisu (npr. 1. stanica gdje se cijela lozinka piše s velikim slovima).
                <br><br>

                Sretno!!!
            </p>
        </div>
        <div class="modal-footer">
            <button id="btn-login" class="modal-action modal-close blue darken-2 waves-effect waves-light btn">ZATVORI</button>
        </div>
    </div>

    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="/js/materialize.min.js"></script>
    <script type="text/javascript">
        var BASE_URL = "{{ url('/') }}/";

        $(document).ready(function() {
            $('.modal-trigger').leanModal();
        });

        $('.button-collapse').sideNav({
            menuWidth: 240,
            edge: 'left',
            closeOnClick: true
        });
    </script>

    @if(App::environment('production'))
        <script type="text/javascript" src="/js/modules.min.js"></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-90553347-1', 'auto');
            ga('send', 'pageview');
        </script>
    @else
        <script type="text/javascript" src="/js/modules.js"></script>
    @endif

    @yield('js-section')
</body>
</html>
