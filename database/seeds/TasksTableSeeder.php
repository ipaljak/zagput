<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $titles = ['Prvi', 'Pritisni gumb', 'Kol\'ko nas ima?', 'Tama skriva znak', 'Pet slova',
            'Jesi li zaključao kapu?', 'Strpljen spašen', 'Rebus', 'Imena', '42 - 29', 'Oblici',
            'Gledaj mene, vidi sebe!', 'Bijela kuća', 'Znakovi', 'Životinje', 'Crno - bijelo', 'L', 'Pravila ne vrijede',
            '&#128083;, &#128054;, &#9993;', 'Boje', 'Zvuk', '16x12', 'Tko je bradonja?', '?', 'Tko nedostaje?'];

        $answers = ['/^ZAGONETKA$/', '/^banana$/', '/^17$/', '/^Batman$/', '/^vatra$/', '/^DIV$/', '/^bogomoljka$/',
            '/^jabuka$/', '/^DiNoSaUr$/', '/^kisik$/', '/^ZEMLJA$/', '/^TEPIH|tepIH$/', '/^Casablanca$/', '/^Konzum$|^KONZUM$/',
            '/^Juraj Dobrila$/', '/^voda$/', '/^STOL$|^stoL$/', '/^playstation/i', '/^zrak$/', '/^balerina$/',
            '/^LEO MESSI$|^LEO MESSi$/', '/^Super Mario$/', '/^Sibir$/', '/^Dida$/', '/^Leeloo$/'];

        $scripts = [NULL, '2.js', NULL, '4.js', NULL, NULL, '7.js', '8.js', NULL, NULL, NULL, NULL, '13.js', NULL,
            NULL, '16.js', '17.js', '18.js', NULL, NULL, '21.js', '22.js', NULL, '24.js', NULL];

        $tasks = [];

        for ($i = 0; $i < sizeof($titles); ++$i) {
            $tasks[] = [
                'id' => $i + 1,
                'title' => $titles[$i],
                'answer' => $answers[$i],
                'script' => $scripts[$i]
            ];
        }

        DB::table('tasks')->insert($tasks);
    }
}
