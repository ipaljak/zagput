<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ImagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $images = [];

        // Task 1
        $images[] = [
            'task_id' => 1,
            'title' => 'task-01-a.png'
        ];

        // Task 2
        $images[] = [
            'task_id' => 2,
            'title' => 'task-02-a.png'
        ];

        $images[] = [
            'task_id' => 2,
            'title' => 'task-02-b.png'
        ];

        // Task 3
        $images[] = [
            'task_id' => 3,
            'title' => 'task-03-a.png'
        ];

        // Task 4
        $images[] = [
            'task_id' => 4,
            'title' => 'task-04-a.png'
        ];

        $images[] = [
            'task_id' => 4,
            'title' => 'task-04-b.png'
        ];

        // Task 5
        $images[] = [
            'task_id' => 5,
            'title' => 'task-05-a.png'
        ];

        // Task 6
        $images[] = [
            'task_id' => 6,
            'title' => 'task-06-a.png'
        ];

        // Task 7
        $images[] = [
            'task_id' => 7,
            'title' => 'task-07-a.jpg'
        ];

        $images[] = [
            'task_id' => 7,
            'title' => 'task-07-b.jpg'
        ];

        // Task 8
        $images[] = [
            'task_id' => 8,
            'title' => 'task-08-a.png'
        ];

        $images[] = [
            'task_id' => 8,
            'title' => 'task-08-b.png'
        ];

        // Task 9
        $images[] = [
            'task_id' => 9,
            'title' => 'task-09-a.png'
        ];

        // Task 10
        $images[] = [
            'task_id' => 10,
            'title' => 'task-10-a.png'
        ];

        // Task 11
        $images[] = [
            'task_id' => 11,
            'title' => 'task-11-a.png'
        ];

        // Task 12
        $images[] = [
            'task_id' => 12,
            'title' => 'task-12-a.png'
        ];

        // Task 13
        $images[] = [
            'task_id' => 13,
            'title' => 'task-13-a.jpg'
        ];

        $images[] = [
            'task_id' => 13,
            'title' => 'task-13-b.jpg'
        ];

        // Task 14
        $images[] = [
            'task_id' => 14,
            'title' => 'task-14-a.png'
        ];

        // Task 15
        $images[] = [
            'task_id' => 15,
            'title' => 'task-15-a.png'
        ];

        // Task 16
        $images[] = [
            'task_id' => 16,
            'title' => 'task-16-a.png'
        ];

        $images[] = [
            'task_id' => 16,
            'title' => 'task-16-b.png'
        ];

        // Task 17
        $images[] = [
            'task_id' => 17,
            'title' => 'task-17-a.png'
        ];

        // Task 18
        $images[] = [
            'task_id' => 18,
            'title' => 'task-18-a.png'
        ];

        $images[] = [
            'task_id' => 18,
            'title' => 'task-18-b.png'
        ];

        $images[] = [
            'task_id' => 18,
            'title' => 'task-18-c.png'
        ];

        $images[] = [
            'task_id' => 18,
            'title' => 'task-18-d.png'
        ];

        $images[] = [
            'task_id' => 18,
            'title' => 'task-18-e.png'
        ];

        // Task 19
        $images[] = [
            'task_id' => 19,
            'title' => 'task-19-a.png'
        ];

        // Task 20
        $images[] = [
            'task_id' => 20,
            'title' => 'task-20-a.png'
        ];

        // Task 21
        $images[] = [
            'task_id' => 21,
            'title' => 'task-21-a.png'
        ];

        // Task 22
        $images[] = [
            'task_id' => 22,
            'title' => 'task-22-a.png'
        ];

        // Task 23
        $images[] = [
            'task_id' => 23,
            'title' => 'task-23-a.png'
        ];

        // Task 24
        $images[] = [
            'task_id' => 24,
            'title' => 'task-24-a.png'
        ];

        $images[] = [
            'task_id' => 24,
            'title' => 'task-24-b.png'
        ];

        $images[] = [
            'task_id' => 24,
            'title' => 'task-24-c.png'
        ];

        $images[] = [
            'task_id' => 24,
            'title' => 'task-24-d.png'
        ];

        // Task 24
        $images[] = [
            'task_id' => 25,
            'title' => 'task-25-a.png'
        ];

        DB::table('images')->insert($images);
    }
}
