<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [];

        for ($i = 1; $i <= 3; ++$i) {
            $users[] = [
                'username' => "user$i",
                'password' => bcrypt('123456')
            ];
        }

        DB::table('users')->insert($users);
    }
}
