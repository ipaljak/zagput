<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FreePuzzlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $puzzles = [
            ['question' => "Zašto slon pojede više hrane u siječnju nego u veljači?", 'answer' => "Zato što siječanj ima više dana od veljače"],
            ['question' => "Što ide gore, a nikad ne dođe dolje?", 'answer' => "Godine"],
            ['question' => "Što svi ljudi svijeta rade u isto vrijeme?", 'answer' => "Stare"],
            ['question' => "Žuto blago u bijeloj kući, nigdje vrata, nigdje prozora, nitko ne može ući. Što je to?", 'answer' => "Žumanjak jajeta"],
            ['question' => "Gdje možete naći rijeke bez vode, šume bez drveća i gradove bez kuća?", 'answer' => "Na zemljopisnoj karti"],
            ['question' => "Što ide kroz gradove i planine, ali nikad se ne pomiče ? ", 'answer' => "Cesta"],
            ['question' => "Što treba slomiti prije nego što se koristi?", 'answer' => "Jaje"],
            ['question' => "Svi me ljudi trebaju, ali svaki dan me bacaju. Što sam ja ? ", 'answer' => "Novac"],
            ['question' => "Što više povlačiš, to sam ja kraća. Što sam ja?", 'answer' => "Cigareta"],
            ['question' => "Odgovaraš mi iako ja nikad ne postavljam pitanja. Tko sam ja ? ", 'answer' => "Telefon"],
            ['question' => "Što radi družba?", 'answer' => "pere kvržice."],
            ['question' => "Na raspolaganju imate dva užeta i upaljač. Poznato je da jedno uže gori točno sat vremena prije no što izgori do kraja. Kako ćete izmjeriti 45 minuta?", 'answer' => "Ako uže zapalimo sa obje strane, izgorjet će za pola sata. Na početku palimo jedno uže s obje strane, a jedno uže samo s jedne strane. Nakon pola sata zapalit ćemo i drugu stranu drugog užeta koje će do kraja izgorjeti nakon 45 minuta."],
            ['question' => "Marko je pozvao Inspektora Slavka jer tvrdi da mu je nestao skupocjeni dijamant. Lopov je navodno razbio prozor, ušao u kuću i ukrao dijamant. Slavko dolazi do Marka te mu objasni da mora još malo pregledati mjesto zločina prije no što započne sa ozbiljnijom istragom. Slavko odluči prošetati oko kuće te u jednom trenu stane na komadić stakla. On pogleda prema kući te primjeti razbijen prozor. Nakon toga on odmah potraži Marka te mu kaže kako ovdje nije bilo nikakve krađe dijamanta, već da je Marko samo htio odštetu. Kako ja Slavko znao da nije bilo krađe ? ", 'answer' => "Slavko je shvatio da je prozor razbijen iznutra pošto su komadići stakla bili s vanjske strane, a lopov je morao prozor razbiti izvana."],
            ['question' => "U ponedjeljak ujutro gospodin Fran nađen je ubijen u svojoj dnevnoj sobi. Utvrđeno je da je umro prije 16 sati, a policija je privela 5 sumnjivaca. Kuhar kaže da je kuhao večeru. Žena kaže da je čitala knjigu. Brat kaže da je gledao tv. Sestra kaže da je baš dobila pismo od muža te ga je čitala. Otac kaže da je uređivao vrt. Policija je malo razmislila te shvatila tko je ubio Frana. Tko je ubojica?", 'answer' => "Ubila ga je sestra. Pošto je tijelo Frana nađeno 16 sati nakon što je umro, može se zaključiti da je Fran umro u nedjelju. U nedjelju ne dolaze pisma što znači da je sestra lagala."],
            ['question' => "Kad kažeš razmakni, one se dotaknu, a kad kažeš dotakni, one se razmaknu. Što je to?", 'answer' => "Usne."],
            ['question' => "Čovjek pokloni dječaku psa, a pas eksplodira. Čovjek se ispriča dječaku te mu da novog psa, a dječak mu zahvali i ode veoma sretan. Zašto dječaku nije bio čudan pas koji je eksplodirao?", 'answer' => "Čovjek je klaun i izrađuje životinje od balona."],
            ['question' => "Mirko kreće na planinu u 10 sati u srijedu i stiže do vrha u 18 sati. Odluči prespavati na vrhu te u četvrtak krene prema dolje istim putem u 10 sati, a dođe dolje u 18 sati. Ako znamo da Mirko nije išao stalnom brzinom ni gore ni dolje, dali možemo reći da postoji točka na kojoj je Mirko bio u isto vrijeme u srijedu i u četvrtak?", 'answer' => "Mirko je sigurno bio na jednoj točki u isto vrijeme. Zamislite da se Mirko u srijedu penje od 10 do 18 sati, a njegov klon se spušta također od 10 do 18 sati. Bez obzira kakvom brzinom njih dvojica išli, postoji mjesto gdje će se sresti."],
            ['question' => "Krešo je nađen mrtav u svom uredu sa nožem u leđima. Detektiv Tin došao je na mjesto zločina te zatekao Matiju ispred Krešinog ureda. Matija je bio Krešin prijatelj te je prvi našao tijelo. Tin je malo bolje pregledao tijelo te u ruci Kreše našao papir. Na papiru je pisalo \"jq58uq j3 7 g89\". Nakon toga odlučio je potražiti sumnjivce. Prvo je ispitao Sinišu, brata od Kreše. On je rekao da nikad nije bio u lošem odnosu sa svojim bratom te da je on bio na poslu cijeli dan. Ivana, žena od Kreše, rekla je kako su se ona i muž taman željeli preseliti u novu kuću te da ne može vjerovati da ga je netko ubio. Nakon mnogo razmišljanja Tin je shvatio tko je ubojica. Tko je ubio Krešu?", 'answer' => "Krešu je ubio Matija.Ako pogledamo papir koji je Tin našao kod Kreše, možete vidjeti da slova na tipkovnici od računala desno ispod slova i brojeva \"jq58uq j3 7g89\" glase \"Matija me ubio\"."],
            ['question' => "Koliko daleko pas može trčati u šumu ?", 'answer' => "Do pola.Tada će trčati van šume."],
            ['question' => "Marija sjedi u svojoj hotelskoj sobi i čita knjigu.Odjednom joj netko pokuca na vrata.Ona ih otvori te ugleda muškarca kojeg nikad prije nije vidjela.On joj reče: \"Oprostite,mislio sam da je ovo moja soba\". Zatim ode niz hodnik te uđe u dizalo.Marija odmah nakon toga pozove osiguranje.Zašto je to učinila ?", 'answer' => "Ljudi ne kucaju sami sebi na vrata od hotelske sobe."],
            ['question' => "Tony Stark mora rasporediti 100 Iron Man figurica u dvije plastične vrećice na način da dvostruko više figurica bude u jednoj vrećici. S obzirom da on nema pojma kako to napravit, vi ste tu da mu pomognete. Kako ćete rasporediti figurice?", 'answer' => "50 figurica u jednu, 50 figurica u drugu. Potom stavimo jednu vrećicu u drugu i stvar je riješena. U jednoj je vrećici 50, a u drugoj 100 figurica."],
            ['question' => "Mirko izađe iz svoje kuće te krene 1 kilometar u smjeru sjevera, okrene se prema zapadu i putuje 3 kilometra. Potom se okrene prema istoku te putuje 2 kilometra. Na kraju se okrene prema jugu te putuje 1 kilometar i završi pred svojom kućom. Kako je to moguće?", 'answer' => "Mirko je otišao u teretanu. Prvo je 3 kilometra odradio na biciklu, a potom 2 kilometra na traci za trčanje."],
            ['question' => "Koja hrvatska riječ pročitana odozada nema k? ", 'answer' => "Kamen"],
            ['question' => "Pegla je u zatvoru, a šešir joj je došao u posjetu. Što se ipak ovdje dešava?", 'answer' => "Igra se Monopoly"],
            ['question' => "Koliko slova ima odgovor na ovo pitanje?", 'answer' => "Tri"]
        ];

        DB::table('free_puzzles')->insert($puzzles);
    }
}
