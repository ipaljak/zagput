<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SolvedTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $solved = [];
        $taskCnt = 24; // always -1 from maximum so we can test the last task

        for ($j = 1; $j <= 3; ++$j) {
            for ($i = 1; $i <= $taskCnt; ++$i) {
                $solved[] = [
                    'task_id' => $i,
                    'user_id' => $j
                ];
            }
        }

        DB::table('solved')->insert($solved);
    }
}
