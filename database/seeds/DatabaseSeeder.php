<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(TasksTableSeeder::class);
//        $this->call(UsersTableSeeder::class);
//        $this->call(SolvedTableSeeder::class);
        $this->call(ImagesTableSeeder::class);
        $this->call(FreePuzzlesSeeder::class);
    }
}
