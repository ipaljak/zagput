$(document).ready(function () {
    var levelInterval = setInterval(function () {
        $('#task-07-a-img').toggleClass('hide');
        $('#task-07-b-img').toggleClass('hide');

        var bugInterval = setInterval(function () {
            $('#task-07-a-img').toggleClass('hide');
            $('#task-07-b-img').toggleClass('hide');
            clearInterval(bugInterval);
        }, 2500);
    }, 50000);

    $(document).mousemove(function () {
        clearInterval(levelInterval);

        levelInterval = setInterval(function () {
            $('#task-07-a-img').toggleClass('hide');
            $('#task-07-b-img').toggleClass('hide');

            var bugInterval = setInterval(function () {
                $('#task-07-a-img').toggleClass('hide');
                $('#task-07-b-img').toggleClass('hide');
                clearInterval(bugInterval);
            }, 2500);
        }, 50000);
    });

    $(document).click(function () {
        clearInterval(levelInterval);

        levelInterval = setInterval(function () {
            $('#task-07-a-img').toggleClass('hide');
            $('#task-07-b-img').toggleClass('hide');

            var bugInterval = setInterval(function () {
                $('#task-07-a-img').toggleClass('hide');
                $('#task-07-b-img').toggleClass('hide');
                clearInterval(bugInterval);
            }, 2500);
        }, 50000);
    });
});