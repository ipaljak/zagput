function generateGrid() {
    var xRatio = $('.task-16-a-img-tag').width() / document.querySelector('.task-16-a-img-tag').naturalWidth;
    var yRatio = $('.task-16-a-img-tag').height() / document.querySelector('.task-16-a-img-tag').naturalHeight;

    var coordStr = (379 * xRatio) + ',' + (254 * yRatio) + ',' + (421 * xRatio) + ',' + (297 * yRatio);

    $('.task').append('\
        <div id="task-map">\
            <map name="task-16-a-map">\
                <area id="task-16-a-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
            <map name="task-16-b-map">\
                <area id="task-16-b-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
        </div>\
    ');

    $('area').click(function () {
        $('#task-16-a-img, #task-16-b-img').toggleClass('hide');
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});