var imagesEnum = {
    0: '#task-24-c-img',
    2: '#task-24-d-img',
    5: '#task-24-b-img',
    9: '#task-24-a-img'
};

var order = [9, 5, 0, 5, 2, 0, 0, 5];

var transitionDelay = 500;  // between each image
var endDelay = 1000;    // at the end

function mod(n, m) {
    return ((n % m) + m) % m;
}

function changeImage(index, delayTime) {
    var transitionInterval = setInterval(function () {
        if (index != 6) {
            $(imagesEnum[order[index]]).toggleClass('hide');
            $(imagesEnum[order[mod(index - 1, order.length)]]).toggleClass('hide');
        }

        clearInterval(transitionInterval);

        // changeImage((index + 1) % order.length, mod(index + 1, order.length) == 0 ? endDelay : transitionDelay);
        changeImage((index + 1) % order.length, transitionDelay);
    }, delayTime);
}

$(window).on("load", function() {
    changeImage(1, transitionDelay);
});