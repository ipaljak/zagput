$(document).ready(function(){
    $('.start-modal-trigger').click(function () {
        HttpRequester.basicGet('session_check', HttpCallback.sessionCheck);
    });

    $('.start-modal-trigger').leanModal({
        ready: function () {
            $('ul.tabs').tabs('select_tab', 'tab-login');
        }
    });

    $('#btn-login').click(function (e) {
        e.preventDefault();

        var data = {
            username: $('#login-username').val(),
            password: $('#login-password').val()
        };

        Toggle.login();
        HttpRequester.basicPost(data, 'login', HttpCallback.login);
    });

    $('#btn-register').click(function (e) {
        e.preventDefault();

        var data = {
            username: $('#reg-username').val(),
            password: $('#reg-password').val(),
            password_confirmation: $('#reg-repassword').val()
        };

        Toggle.register();
        HttpRequester.basicPost(data, 'registration', HttpCallback.register);
    });
});