function generateGrid() {
    var xRatio = $('.task-13-a-img-tag').width() / document.querySelector('.task-13-a-img-tag').naturalWidth;
    var yRatio = $('.task-13-a-img-tag').height() / document.querySelector('.task-13-a-img-tag').naturalHeight;

    var coordStr = (341 * xRatio) + ',' + (18 * yRatio) + ',' + (377 * xRatio) + ',' + (40 * yRatio);

    $('.task').append('\
        <div id="task-map">\
            <map name="task-13-a-map">\
                <area id="task-13-a-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
            <map name="task-13-b-map">\
                <area id="task-13-b-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
        </div>\
    ');

    $('area').click(function () {
        $('#task-13-a-img, #task-13-b-img').toggleClass('hide');
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});