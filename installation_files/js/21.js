function generateGrid() {
    var xRatio = $('.task-21-a-img-tag').width() / document.querySelector('.task-21-a-img-tag').naturalWidth;
    var yRatio = $('.task-21-a-img-tag').height() / document.querySelector('.task-21-a-img-tag').naturalHeight;

    var coordStr = (502 * xRatio) + ',' + (218 * yRatio) + ',' + (742 * xRatio) + ',' + (339 * yRatio);

    $('.task').append('\
        <div id="task-map">\
            <map name="task-21-a-map">\
                <area id="task-21-a-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
            <map name="task-21-b-map">\
        </div>\
    ');

    $('area').click(function () {
        $('#task-21-audio').get(0).play();
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    // Append audio file
    $('.task').prepend('<audio id="task-21-audio" src="/audio/task-21-a.wav" type="audio/wav"></audio>');

    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});
