function generateGrid() {
    var xRatio = $('.task-08-a-img-tag').width() / document.querySelector('.task-08-a-img-tag').naturalWidth;
    var yRatio = $('.task-08-a-img-tag').height() / document.querySelector('.task-08-a-img-tag').naturalHeight;

    var coordStr = (101 * xRatio) + ',' + (91 * yRatio) + ',' + (250 * xRatio) + ',' + (201 * yRatio);

    $('.task').append('\
        <div id="task-map">\
            <map name="task-08-a-map">\
                <area id="task-08-a-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
            <map name="task-08-b-map">\
                <area id="task-08-b-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
        </div>\
    ');

    $('area').mouseenter(function () {
        $('#task-08-a-img').addClass('hide');
        $('#task-08-b-img').removeClass('hide');
    }).mouseleave(function () {
        $('#task-08-b-img').addClass('hide');
        $('#task-08-a-img').removeClass('hide');
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});