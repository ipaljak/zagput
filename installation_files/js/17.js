var elementCounter = 0;

function appendArea(x1, y1, x2, y2) {
    $('#task-map').append('<area id="task-17-a-map-' + elementCounter++ + '" ' +
        'shape="rect" coords=' + x1 + ',' + y1 + ',' + x2 + ',' + y2 + ' />');
}

function generateGrid() {
    var xRatio = $('.task-17-a-img-tag').width() / document.querySelector('.task-17-a-img-tag').naturalWidth;
    var yRatio = $('.task-17-a-img-tag').height() / document.querySelector('.task-17-a-img-tag').naturalHeight;

    var xOffset = 12 * xRatio;
    var yOffset = 35 * yRatio;

    var startCoords = {"x1": 20 * xRatio, "y1": 16 * yRatio, "x2": 30 * xRatio, "y2": 36 * yRatio};
    var uniqueClicks = {};

    var letterWidth = 12 * xRatio;
    var letterHeight = 20 * yRatio;

    $('.task').prepend('<map id="task-map" name="task-17-a-map"></map>');

    for (var i = 0; i < 9; ++i) {
        if (i >= 2) { // Generates last 7 rows
            for (var j = 0; j < 2; ++j) {
                appendArea(startCoords.x1 + j * xOffset, startCoords.y1 + i * yOffset,
                    startCoords.x2 + j * xOffset, startCoords.y2 + i * yOffset);
            }

            for (var j = 34; j < 36; ++j) {
                appendArea(startCoords.x1 + j * xOffset, startCoords.y1 + i * yOffset,
                    startCoords.x2 + j * xOffset, startCoords.y2 + i * yOffset);
            }

        } else { // Generates first 2 rows
            for (var j = 0; j < 36; ++j) {
                appendArea(startCoords.x1 + j * xOffset, startCoords.y1 + i * yOffset,
                    startCoords.x2 + j * xOffset, startCoords.y2 + i * yOffset);
            }
        }
    }

    $('area').click(function () {
        if (Object.keys(uniqueClicks).length == 100) {
            $('.task-17-a-img').attr('src', '/images/task-17-b.png');
        }

        $('.the-L').remove();

        var coords = $(this).attr('coords').split(',');

        uniqueClicks[coords] = true;

        $('.task-17-a-img').append(
            '<img class="the-L" src="/images/task-17-L.png"\
                  style="position:absolute; left:' + coords[0] + 'px; top:' + coords[1] + 'px; width: ' + letterWidth + 'px; height: ' + letterHeight + 'px;" />');
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});