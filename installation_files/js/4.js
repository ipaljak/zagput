function generateGrid() {
    var xRatio = $('.task-04-a-img-tag').width() / document.querySelector('.task-04-a-img-tag').naturalWidth;
    var yRatio = $('.task-04-a-img-tag').height() / document.querySelector('.task-04-a-img-tag').naturalHeight;

    var coordStr = (1433 * xRatio) + ',' + (291 * yRatio) + ',' + (1611 * xRatio) + ',' + (389 * yRatio);

    $('.task').append('\
        <div id="task-map">\
            <map name="task-04-a-map">\
                <area id="task-04-a-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
            <map name="task-04-b-map">\
                <area id="task-04-b-map" shape="rect" coords="' + coordStr + '" />\
            </map>\
        </div>\
    ');

    $('area').click(function () {
        $('#task-04-a-img, #task-04-b-img').toggleClass('hide');
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});