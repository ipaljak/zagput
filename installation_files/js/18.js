var toggleBack = "";

function generateGrid() {
    $('#task-18-a-img').remove();

    var xRatio = $('#task-18-hash').width() / document.querySelector('#task-18-hash').naturalWidth;
    var yRatio = $('#task-18-hash').height() / document.querySelector('#task-18-hash').naturalHeight;

    var coordsTop = (11 * xRatio) + ',' + (3 * yRatio) + ',' + (16 * xRatio) + ',' + (9 * yRatio);
    var coordsLeft = (16 * xRatio) + ',' + (11 * yRatio) + ',' + (21 * xRatio) + ',' + (17 * yRatio);
    var coordsBottom = (8 * xRatio) + ',' + (19 * yRatio) + ',' + (13 * xRatio) + ',' + (27 * yRatio);
    var coordsRight = (3 * xRatio) + ',' + (11 * yRatio) + ',' + (8 * xRatio) + ',' + (17 * yRatio);

    $('.task').append('\
        <map id="task-map" name="task-18-hash-map">\
            <area id="task-18-area-e" imageId="#task-18-e-img" shape="rect" coords="' + coordsTop + '" />\
            <area id="task-18-area-c" imageId="#task-18-c-img" shape="rect" coords="' + coordsRight + '" />\
            <area id="task-18-area-b" imageId="#task-18-b-img" shape="rect" coords="' + coordsBottom + '" />\
            <area id="task-18-area-d" imageId="#task-18-d-img" shape="rect" coords="' + coordsLeft + '" />\
        </map>\
    ');

    $('area').click(function () {
        var imageId = $(this).attr('imageId');

        $(toggleBack).toggleClass('hide');
        $(imageId).toggleClass('hide');

        toggleBack = imageId;
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});