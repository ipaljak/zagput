HttpRequester = {
    basicGet: function(_url, callback) {
        $.ajax({
            type: 'GET',
            dataType: 'json',
            url: BASE_URL + _url,
            timeout: 2000,

            beforeSend: function (x) {
                x.setRequestHeader('Content-Type', 'application/json');
            },

            complete: function (xhr) {
                callback(xhr.status, xhr.responseText);
            }
        });
    },

    basicPost: function(_data, _url, callback) {
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: BASE_URL + _url,
            timeout: 2000,
            data: JSON.stringify(_data),

            beforeSend: function (x) {
                x.setRequestHeader('Content-Type', 'application/json');
                x.setRequestHeader('X-CSRF-TOKEN', $('#' + _url + '-csrf-token').attr('value'));
            },

            complete: function (xhr) {
                callback(xhr.status, xhr.responseText);
            }
        });
    },

    sendTokenGet: function (_url) {
        $.ajax({
            type: "GET",
            url: BASE_URL + _url,

            beforeSend: function (x) {
                x.setRequestHeader('Authorization', localStorage.getItem('token'));
            },

            success: function (data, status, request) {
                localStorage.setItem('token', request.getResponseHeader('Authorization'));
            }
        });
    },

    sendTokenPost: function (_url, _data) {
        $.ajax({
            type: "POST",
            dataType: 'json',
            url: BASE_URL + _url,
            data: JSON.stringify(_data),

            beforeSend: function (x) {
                x.setRequestHeader('Content-Type', 'application/json');
                x.setRequestHeader('Authorization', localStorage.getItem('token'));
            },

            success: function (data, status, request) {
                localStorage.setItem('token', request.getResponseHeader('Authorization'));
            }
        });
    }
}

HttpCallback = {
    sessionCheck: function(status, response) {
        if (status === 200) {
            window.location.href = BASE_URL + 'task';
        }
    },

    login: function (status, response) {
        Toggle.login();

        if (status === 200) {
            $('#login-error').addClass('hide');
            window.location.href = BASE_URL + 'task';

        } else {
            $('#login-error').removeClass('hide');
        }
    },

    register: function (status, response) {
        Toggle.register();

        if (status === 200) {
            $('#registration-error').addClass('hide');
            $('#registration-success').removeClass('hide');

            window.location.href = BASE_URL + 'task';

        } else if (status === 201) {
            $('#registration-error').addClass('hide');
            $('#registration-success').removeClass('hide');

        } else {
            $('#registration-success').addClass('hide');
            $('#registration-error').removeClass('hide');
        }
    }
}

Toggle = {
    login: function () {
        $('#btn-tab-registration').toggleClass('disabled');
        $('#login-fields').toggleClass('hide');
        $('#preloader').toggleClass('hide');
    },

    register: function () {
        $('#btn-tab-login').toggleClass('disabled');
        $('#register-fields').toggleClass('hide');
        $('#preloader').toggleClass('hide');
    }
}