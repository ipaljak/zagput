function generateGrid() {
    var grid = ['B', 'B', 'B', 'C', 'C', 'C', 'C', 'C', 'B', 'B', 'B', 'B', 'B', 'B', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
        'C', 'C', 'B', 'B', 'B', 'S', 'S', 'S', 'K', 'K', 'Q', 'K', 'B', 'B', 'B', 'B', 'S', 'K', 'S', 'K', 'K', 'K',
        'Q', 'K', 'K', 'K', 'B', 'B', 'S', 'K', 'S', 'S', 'K', 'K', 'K', 'S', 'K', 'K', 'K', 'B', 'S', 'S', 'K', 'K',
        'K', 'K', 'S', 'S', 'S', 'S', 'B', 'B', 'B', 'B', 'K', 'K', 'K', 'K', 'K', 'K', 'K', 'B', 'B', 'B', 'B', 'P',
        'P', 'C', 'P', 'P', 'P', 'B', 'B', 'B', 'B', 'B', 'P', 'P', 'P', 'C', 'P', 'P', 'C', 'P', 'P', 'P', 'B', 'P',
        'P', 'P', 'P', 'C', 'P', 'P', 'C', 'P', 'P', 'P', 'P', 'K', 'K', 'P', 'P', 'C', 'C', 'C', 'C', 'P', 'P', 'K',
        'K', 'K', 'K', 'K', 'C', 'Z', 'C', 'C', 'Z', 'C', 'K', 'K', 'K', 'K', 'K', 'C', 'C', 'C', 'C', 'C', 'C', 'C',
        'C', 'K', 'K', 'B', 'B', 'C', 'C', 'C', 'B', 'B', 'C', 'C', 'C', 'B', 'B', 'B', 'S', 'S', 'S', 'B', 'B', 'B',
        'B', 'S', 'S', 'S', 'B', 'S', 'S', 'S', 'S', 'B', 'B', 'B', 'B', 'S', 'S', 'S', 'S'];

    var xOffset = 60;
    var yOffset = 80;
    var elementsCounter = 0;

    var xSize = $('.task-22-a-img-tag').width();
    var ySize = $('.task-22-a-img-tag').height();

    var xRatio = xSize / document.querySelector('.task-22-a-img-tag').naturalWidth;
    var yRatio = ySize / document.querySelector('.task-22-a-img-tag').naturalHeight;

    console.log(xSize + 'x' + ySize);

    xOffset *= xRatio;
    yOffset *= yRatio;

    var startCoords = {"x1": 0, "y1": 0, "x2": xOffset, "y2": yOffset};
    var coords = {};

    $('.task').prepend('<map id="task-map" name="task-22-a-map"></map>');

    for (var i = 0; i < 16; ++i) {
        for (var j = 0; j < 12; ++j) {
            var coord = (startCoords.x1 + j * xOffset) + ',' + (startCoords.y1 + i * yOffset) + ',' +
                (startCoords.x2 + j * xOffset) + ',' + (startCoords.y2 + i * yOffset);

            coords[coord] = elementsCounter;

            $('#task-map').append('<area id="task-22-a-map-' + elementsCounter++ + '" shape="rect" coords="' + coord + '" />');
        }
    }

    $('area').click(function () {
        var coord = $(this).attr('coords');
        var colour = '';

        switch (grid[coords[coord]]) {
            // Red
            case 'C':
                colour = 'red';
                break;

            // White
            case 'B':
                colour = 'white';
                break;

            // Brown
            case 'S':
                colour = '#663300';
                break;

            // Skin colour
            case 'K':
                colour = '#ffe39f';
                break;

            // Yellow
            case 'Z':
                colour = 'yellow';
                break;

            // Blue
            case 'P':
                colour = 'blue';
                break;

            // Black
            case 'Q':
                colour = 'black';
                break;

            default:
                return;
        }

        // Set colour div size
        $('.task-22-a-img').css('width', ($('.task-22-a-img-tag').width() + 2));
        $('.task-22-a-img').css('height', ($('.task-22-a-img-tag').height() + 2));

        // Hide image and show colour grid
        $('.task-22-a-img-tag').toggleClass('hide');
        $('.task-22-a-img').css('background-color', colour);

        // Reset back to normal
        var colourInterval = setInterval(function () {
            $('.task-22-a-img-tag').toggleClass('hide');
            $('.task-22-a-img').css('background-color', '');

            // Reset colour div size
            $('.task-22-a-img').css('width', '');
            $('.task-22-a-img').css('height', 'auto');

            clearInterval(colourInterval);
        }, 500);
    });
}

function resizeEnds() {
    if (new Date() - rtime < delta) {
        setTimeout(resizeEnds, delta);
    } else {
        timeout = false;

        $('#task-map').remove(); // Clear old map areas
        generateGrid();
    }
}

var rtime;
var timeout = false;
var delta = 200;

$(window).on("load", function() {
    // Add border to img div
    $('.task-22-a-img').css('border', '1px solid black');

    generateGrid();

    $(window).resize(function() {
        rtime = new Date();
        if (timeout === false) {
            timeout = true;
            setTimeout(resizeEnds, delta);
        }
    });
});