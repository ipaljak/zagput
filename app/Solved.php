<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solved extends Model
{
    protected $table = 'solved';

    protected $primaryKey = ['user_id', 'task_id'];

    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
