<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    protected $fillable = ['username', 'password'];

    protected $hidden = ['password', 'remember_token'];

    public function solved()
    {
        return $this->hasMany('App\Solved');
    }

    public static function login($credentials)
    {
        $ret = Auth::attempt($credentials);

        if ($ret) {
            $user = Auth::user();
            $user->ip_address = request()->ip();
            $user->last_login = Carbon::now();
            $user->save();
        }

        return $ret;
    }
}
