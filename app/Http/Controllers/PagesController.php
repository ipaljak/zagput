<?php

namespace App\Http\Controllers;

use App\FreePuzzle;
use App\Solved;
use App\Task;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Debug\Exception\FatalThrowableError;

/**
 * @Middleware("web")
 */
class PagesController extends Controller
{
    /**
     * @GET("/", as="index")
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        return view('pages.index');
    }

    /**
     * @GET("/task", as="get.task", middleware="auth")
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getTask(Request $request)
    {
        $solved = Solved::where('user_id', $request->user()->id)->orderBy('task_id', 'desc')->first();

        if ($solved !== NULL && $solved['task_id'] != MAX_LEVEL) {
            $taskId = $solved['correct_answer'] == true ? $solved['task_id'] + 1 : $solved['task_id'];

            return redirect("/task/$taskId");
        }

        return redirect('/task/1');
    }

    /**
     * @GET("/task/{id}", as="show.task", middleware="auth")
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showTask(Request $request, $id)
    {
        if ($id != 1 && Solved::where([['user_id', '=', $request->user()->id], ['task_id', '=', $id - 1]])->first()['correct_answer'] == true) {
            return view('pages.task', Task::where('id', $id)->with('images')->first());
        }
        else if ($id == 1) {
            return view('pages.task', Task::where('id', $id)->with('images')->first());
        }

        return redirect('/');
    }

    /**
     * @POST("/task/{id}", as="task", middleware="auth")
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postTask(Request $request, $id)
    {
         // Checks if someone is trying to skip tasks
        if ($id != 1 && Solved::where([['user_id', '=', $request->user()->id], ['task_id', '=', $id - 1]])->first()['correct_answer'] != true) {
            return redirect('/');
        }

        $data = $request->only(['answer']);
        $task = Task::find($id);
        $solved = Solved::where([['user_id', '=', $request->user()->id], ['task_id', '=', $id]])->first();

        if ($task !== NULL) {
            // Create row if first time
            if ($solved === NULL) {
                $solved = new Solved;

                $solved->user_id = $request->user()->id;
                $solved->task_id = $id;

                $solved->save();
            }

            if (preg_match($task['answer'], $data['answer'])) {
                // If already solved, keep the original solved time, e.g. skip the next step
                if ($solved->correct_answer == true) {
                    if ($solved->task_id != MAX_LEVEL) {
                        return redirect('/task/' . ($id + 1));
                    } else {
                        return redirect('/congratz');
                    }
                }

                // Answer was correct
                DB::table('solved')
                    ->where([['user_id', '=', $request->user()->id], ['task_id', '=', $id]])
                    ->update(['correct_answer' => true, 'updated_at' => Carbon::now()]);

                if ($id != MAX_LEVEL) {
                    $id = $id + 1; // redirect to next task
                } else {
                    return redirect('/congratz');
                }
            } else {
                // If already solved, keep the original solved time, e.g. skip the next step
                if ($solved->correct_answer == true) {
                    return redirect("/task/$id");
                }

                // Count the wrong try
                DB::table('solved')
                    ->where([['user_id', '=', $request->user()->id], ['task_id', '=', $id]])
                    ->update(['wrong_tries' => $solved->wrong_tries + 1, 'updated_at' => Carbon::now()]);
            }
        }

        return redirect("/task/$id");
    }

    /**
     * @GET("/hall-of-fame", as="hall.of.fame")
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getHallOfFame()
    {
        // TODO add wrong tries factor
        $ranks = DB::select('SELECT a.user_id, u.username, a.task_id, b.updated_at, b.correct_answer FROM (
                                SELECT user_id, MAX(task_id) as task_id FROM solved a GROUP BY user_id
                            ) as a
                                LEFT JOIN solved b ON a.user_id
                                LEFT JOIN users u ON a.user_id
                                    WHERE a.user_id = u.id AND a.user_id = b.user_id AND a.task_id = b.task_id
                                GROUP BY a.user_id, b.updated_at
                                ORDER BY a.task_id DESC, b.correct_answer DESC, b.updated_at ASC');

        return view('pages.hall-of-fame', ['ranks' => $ranks]);
    }

    /**
     * @GET("/congratz", as="congratz", middleware="auth")
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function getCongratz(Request $request)
    {
        $solved = Solved::where('user_id', $request->user()->id)->orderBy('task_id', 'desc')->first();

        if ($solved !== NULL && $solved['task_id'] == MAX_LEVEL && $solved['correct_answer'] == true) {
            return view('pages.congratz');
        }

        return redirect('/');
    }

    /**
     * @GET("/puzzles", as="puzzles")
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPuzzles() {
        return view('pages.puzzles', ['puzzles' => FreePuzzle::all()]);
    }

    /**
     * @GET("/logout", as="logout", middleware="auth")
     */
    public function getLogout()
    {
        Auth::logout();

        return redirect('');
    }
}