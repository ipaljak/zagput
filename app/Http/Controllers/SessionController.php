<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response as ResponseHTTP;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\MessageBag;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class SessionController extends Controller
{

    /**
     * @GET("/session_check", as="session.check", middleware="web")
     *
     * @return mixed
     */
    public function sessionExists()
    {
        $ret['response']['message'] = Auth::check();

        return $ret['response']['message'] ?
            response()->json($ret, ResponseHTTP::HTTP_OK) :
            response()->json($ret, ResponseHTTP::HTTP_BAD_REQUEST);
    }

    /**
     * @POST("/login", as="basic.login", middleware="web")
     *
     * @param Request $request
     * @return Response
     */
    public function basicLogin(Request $request)
    {
        // TODO validator

        $data = $request->only('username', 'password');

        if (User::login($data)) {
            $ret['response']['message'] = "Login successful!";
            return response()->json($ret, ResponseHTTP::HTTP_OK);
        }

        $ret['response']['message'] = "Wrong username or password.";

        return response()->json($ret, ResponseHTTP::HTTP_BAD_REQUEST);
    }

    /**
     * @POST("/api/login", as="api.login")
     *
     * @param Request $request
     * @return json
     */
    public function tokenLogin(Request $request)
    {
        $data = $request->only('username', 'password');

        if ($token = JWTAuth::attempt($data)) {
            $ret['response']['status'] = 1;
            $ret['response']['user'] = Auth::user();

            return response()->json($ret, ResponseHTTP::HTTP_OK, ['Authorization' => 'Bearer ' . $token]);
        }

        $ret['response']['status'] = 0;

        try {
            JWTAuth::attempt($data);

        } catch (JWTException $e) {
            $ret['response']['debug'] = $e;
        }

        $ret['response']['message'] = "Wrong username or password.";

        return response()->json($ret, ResponseHTTP::HTTP_BAD_REQUEST);
    }

    /**
     * @GET("/api/token-refresh", as="api.refresh-token", middleware="jwt.refresh")
     *
     * @param Request $request
     */
    public function tokenRefresh(Request $request)
    {
        //token refresh
    }
}
