<?php

namespace App\Http\Controllers;

use App\Helpers\ValidationHelper;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response as ResponseHTTP;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class UserController extends Controller
{
    /**
     * @POST("/registration", as="basic.registration", middleware="web")
     *
     * @param Request $request
     * @return mixed
     */
    public function basicRegistration(Request $request)
    {
        $data = $request->only('username', 'password', 'password_confirmation');
        $validator = ValidationHelper::registration($data);

        if ($validator->passes()) {
            $data['password'] = bcrypt($data['password']);

            User::create($data);

            $credentials = $request->only('username', 'password');

            if (User::login($credentials)) {
                $ret['response']['message'] = "Registration successful! You were logged in automatically.";
                return response()->json($ret, ResponseHTTP::HTTP_OK);
            }

            $ret['response']['message'] = "Registration successful!";

            return response()->json($ret, ResponseHTTP::HTTP_CREATED);
        }

        $ret['response']['message'] = $validator->messages();

        return response()->json($ret, ResponseHTTP::HTTP_BAD_REQUEST);
    }

    /**
     * @POST("/api/registration", as="api.registration")
     *
     * @param Request $request
     * @return json
     */
    public function apiRegistration(Request $request)
    {
        $data = $request->only('username', 'password', 'password_confirmation');
        $validator = ValidationHelper::registration($data);

        if ($validator->passes()) {
            $data['password'] = bcrypt($data['password']);;

            User::create($data);

            $ret['response']['status'] = 1;
            $ret['response']['message'] = "Registration successful!";

            return Response::json($ret, ResponseHTTP::HTTP_OK);
        }

        $ret['response']['status'] = 0;
        $ret['response']['message'] = $validator->messages();

        return Response::json($ret, ResponseHTTP::HTTP_BAD_REQUEST);
    }
}
