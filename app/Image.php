<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function task()
    {
        return $this->belongsTo('App\Task');
    }
}
