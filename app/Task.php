<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $primaryKey = 'id';

    protected $hidden = ['deleted_at', 'created_at', 'updated_at'];

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function solved()
    {
        return $this->hasMany('App\Solved');
    }
}
