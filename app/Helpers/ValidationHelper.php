<?php
/**
 * Created by PhpStorm.
 * User: mjurinic
 * Date: 8/20/16
 * Time: 2:45 PM
 */

namespace app\Helpers;

use Illuminate\Support\Facades\Validator;

class ValidationHelper
{
    private static $messages = [
        'required' => 'Polje je obavezno.',
        'min' => ':attribute mora imati najmanje :min znakova.',
        'regex' => ':attribute mora imati između 3 i 20 znakova i smije sadržavati samo brojeve, slova i točku.',
        'confirmed' => 'Lozinke se ne podudaraju.'
    ];

    public static function registration($data)
    {
        $rules = [
            'username' => 'required|regex:/^[a-zA-Z0-9.]{3,20}$/|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6'
        ];

        return Validator::make($data, $rules, self::$messages);
    }
}